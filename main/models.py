from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class Job(models.Model):
  image = models.ImageField(upload_to='images/')
  summary = models.CharField(max_length=200)

class Skill(models.Model):
  name = models.CharField(max_length=50)
  level = models.IntegerField(validators=[MaxValueValidator(5), MinValueValidator(1)])
  def __str__(self):
    return self.name
  def displayLevel(self):
    return '★' * self.level + '✩' * (5 - self.level)

class Certi(models.Model):
  imageurl = models.URLField()
  detailurl = models.URLField()
