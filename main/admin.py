from django.contrib import admin
from .models import Job, Skill, Certi

# Register your models here.
admin.site.register(Job)
admin.site.register(Skill)
admin.site.register(Certi)