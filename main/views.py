from django.shortcuts import render
from .models import Job, Skill, Certi

# Create your views here.
def main(request):
  jobs = Job.objects
  skills = Skill.objects
  certis = Certi.objects
  return render(request, 'main.html', {'jobs':jobs, 'skills':skills, 'certis':certis})