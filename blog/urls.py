from django.urls import path
from . import views

urlpatterns = [
  path('', views.blogHome, name='blogHome'),
  path('<int:blog_id>', views.blogDetail, name='blogDetail'),
]