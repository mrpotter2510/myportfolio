from django.shortcuts import render, redirect, get_object_or_404
from .models import Blog, Comment

# Create your views here.
def blogHome(request):
  blogs = Blog.objects.order_by('-pubDate')
  return render(request, 'blogList.html', {'blogs':blogs})

def blogDetail(request, blog_id):
  blog = get_object_or_404(Blog, pk=blog_id)
  comments = Comment.objects.filter(blogID=blog)
  
  if request.method == 'GET':
    return render(request, 'blogDetail.html', {'blog':blog, 'comments':comments})

  elif request.method == 'POST':
    if request.POST['name'] and request.POST['text']:
      comment = Comment()
      comment.name = request.POST['name']
      comment.text = request.POST['text']
      comment.blogID = blog
      comment.save()
      return redirect('/blog/%d' % (blog_id))
    else:
      return render(request, 'blogDetail.html', {'blog':blog, 'comments':comments, 'error':'All fields should be filled.'})
  