from django.db import models

# Create your models here.
class Blog(models.Model):
  title = models.CharField(max_length=255)
  pubDate = models.DateTimeField()
  image = models.ImageField(upload_to='images/')
  body = models.TextField()

  def __str__(self):
    return '%d - %s' % (self.id, self.title)

  def summary(self):
    splitIndex = self.body[0: 100].rindex(' ')
    return self.body[0: splitIndex] + '...'

  
class Comment(models.Model):
  name = models.CharField(max_length=30)
  text = models.TextField()
  time = models.DateTimeField(auto_now=True)
  blogID = models.ForeignKey(Blog, on_delete=models.CASCADE)
